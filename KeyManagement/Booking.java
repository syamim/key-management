
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.text.*;
import java.util.Date;

public class Booking {
  Scanner console = new Scanner(System.in);
  String start_date, end_date, username, name;
  Applicant applicant = new Applicant();
  Admin admin = new Admin();

  // ******** Admin Controller *********//
  public void approveBooking() throws IOException {
    Color print = new Color();
    Table table = new Table();
    table.adminPrintTable("booking.csv", 0);
    print.inputLabel("Enter number of application to be approve : ");
    String selected = console.nextLine();
    // saving
    String line = "";
    String[] booksStore = new String[]{};
    BufferedReader list = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/data/booking.csv"));
    while ((line = list.readLine()) != null) {
      booksStore = ArrayHelper.push(booksStore,line);
    }
    list.close();
    
    String temp = booksStore[Integer.parseInt(selected)];
    String[] tempArr = temp.split(",");
    tempArr[3] = "true";
    booksStore[Integer.parseInt(selected)] = tempArr[0] + "," + tempArr[1] + "," + tempArr[2] + "," + tempArr[3];
    // edit data done
    // saving back to file
    PrintWriter booksFile = new PrintWriter(System.getProperty("user.dir") + "/data/booking.csv");
    for (int i = 0; i < booksStore.length; i++) {
      booksFile.println(booksStore[i]);
    }
    booksFile.close();
    // done saving
    table.adminPrintTable("booking.csv", Integer.parseInt(selected));
    print.successLabel("Application approved !\n");
    admin.menu();
  }
  
  // generate report module
  public void generateReport() throws FileNotFoundException {
    PrintWriter report = new PrintWriter(System.getProperty("user.dir") + "/data/report/booking_report.txt");
    report.println("Admin Application Report");
    report.println("");
    report.println("Number of Approved Application : " + calculateApplication("true"));
    String line = "";
    String splitBy = ",";
    try {
      BufferedReader booking_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/booking.csv"));
      String tableHeader = booking_list.readLine(); //saja remove
      while ((line = booking_list.readLine()) != null) {
        String[] item = line.split(splitBy);
        if (item[3].equals("true")){
          report.println("Name : " + item[0] + " || Start Date : " + item[1] + " || End Date " + item[2] + " || Aprroved");
        }
      }
      report.println("");
      report.println("Number of Unapproved Application : " + calculateApplication("false"));
      BufferedReader booking_list2 = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/booking.csv"));
      String tableHeader2 = booking_list2.readLine(); //saja remove
      while ((line = booking_list2.readLine()) != null) {
        String[] item = line.split(splitBy);
        if (item[3].equals("false")){
          report.println("Name : " + item[0] + " || Start Date : " + item[1] + " || End Date " + item[2] + " || Unaprroved");
        }
      }
      booking_list2.close();
      booking_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    report.close();
    admin.menu();
  }
  
  public Integer calculateApplication(String value){
    String line = "";
    String splitBy = ",";
    Integer total = 0;
    try {
      BufferedReader booking_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/booking.csv"));
      booking_list.readLine(); //saja remove
      while ((line = booking_list.readLine()) != null) {
        String[] booking = line.split(splitBy);
        if (booking[3].equals("true")){
          total++;
        }else if(booking[3].equals("false")){
          total++;
        }
      }
      booking_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return total;
  }
  
  //******** Applicant Controller *********/ //
  // Applicant Booking Module
  public void addBooking(String username, String name) {
    Color print = new Color();
    this.username = username;
    this.name = name;
    print.inputLabel("Enter date when you want to book (DD-MM-YYYY): ");
    this.start_date = console.nextLine();
    print.inputLabel("Enter date ");
    print.warningLabel("until");
    print.inputLabel(" when you want to book (DD-MM-YYYY): ");
    this.end_date = console.nextLine();
    isAvailable();
    this.applicant.menu();
  }

  // check gym availbility
  public boolean isAvailable() {
    Color print = new Color();
    Boolean date_taken = false;
    try {
      date_taken = readBookingList();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    if (date_taken){
      print.errorLabel("Date has already taken!\n");
    }else{
      print.successLabel("Date is available!\n");
      print.successLabel("Add Booking Now....\n");
      try {
        saveBooking(this.username);
        print.successLabel("Date Locked\n");
        applicant.menu();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return date_taken;
  }

  public boolean compareDate(String start_existing, String end_existing) throws ParseException, IOException {
    SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
    Date start1 = sdformat.parse(this.start_date);
    Date end1 = sdformat.parse(this.end_date);
    Date start2 = sdformat.parse(start_existing);
    Date end2 = sdformat.parse(end_existing);
    return start1.getTime() <= end2.getTime() && start2.getTime() <= end1.getTime(); 
  }

  public boolean readBookingList() throws ParseException {
    boolean isOverlape = false;
    String line = "";
    String splitBy = ",";
    try {
      BufferedReader booking_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/booking.csv"));
      String tableHeader = booking_list.readLine(); //saja remove
      while ((line = booking_list.readLine()) != null) {
        String[] booking = line.split(splitBy);
        String start_existing = booking[1];
        String end_existing = booking[2];
        isOverlape = compareDate(start_existing, end_existing);
      }
      booking_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return isOverlape;
  }
  
  public void saveBooking(String username) throws IOException {
    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/data/booking.csv",true);
    PrintWriter booking_list = new PrintWriter(path);
    booking_list.println(this.name + "," + this.start_date + "," + this.end_date + ",false");
    booking_list.close();    
  }
  
  public void printBookingSlip(String username, String name){
    Color print = new Color();
    Table table = new Table();
    table.printTableUser("booking.csv",name,0);
    print.inputLabel("Enter booking number to print : ");
    String slip_num = console.nextLine();
    table.printTableUser("booking.csv",name,Integer.parseInt(slip_num));
    applicant.menu();
  }
}