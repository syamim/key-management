
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;


public class Main {
  Scanner console = new Scanner(System.in);
  Boolean logged_in;
  String user_type;
  Applicant applicant = new Applicant();
  Admin admin = new Admin();

  public static void main(String[] args) {
    Color print = new Color();
    Main temp_main = new Main();

    clearConsole();
    clearCurrentUser();
    
    try {
      System.out.print(banner());
      System.out.println("");
    } catch (Exception e) {
      print.successLabel("  Welcome to Gym Management System\n");
      print.errorLabel("======================================\n");
    }

    
    do {
      temp_main.logged_in = temp_main.login();
    } while (temp_main.logged_in == false);

    if (temp_main.user_type.equals("applicant")) {
      temp_main.applicant.ApplicantMain();
    } else { // user type == admin
      temp_main.admin.adminMain();
    }
  }

  public static void clearCurrentUser(){
    // set user in text file to able to move data freely
    PrintWriter writer;
    try {
      writer = new PrintWriter(System.getProperty("user.dir") + "/data/currentUser.txt");
      writer.print("");
      writer.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  public Boolean login() {
    Color print = new Color();
    Boolean logged_in = false;
    print.normalLabel ("  Select option \n");
    print.successLabel("≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠\n");
    print.warningLabel("1. Log in as Applicant \t 2. Log in as Admin \t 3. Register as Applicant\n");
    print.inputLabel("Enter number : ");
    String user = console.nextLine();
    if (user.equals("1")) {
      logged_in = verifyApplicant();
      this.user_type = "applicant";
    } else if (user.equals("2")){
      logged_in = verifyAdmin();
      this.user_type = "admin";
    }else if(user.equals("3")){
      logged_in = false;
      try {
        registerApplicant();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return logged_in;
  }

  public void registerApplicant() throws IOException {
    Color print = new Color();
    print.inputLabel("Enter username : ");
    String temp_username = console.nextLine();
    print.inputLabel("Enter name : ");
    String temp_name = console.nextLine();
    print.inputLabel("Enter Matric Number [BCS1901-023] : ");
    String temp_mat = console.nextLine();
    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/data/applicant.csv",true);
    PrintWriter booking_list = new PrintWriter(path);
    booking_list.println(temp_username + "," + temp_mat + "," + temp_name);
    booking_list.close();
  }

  public Boolean verifyApplicant() {
    Color print = new Color();
    print.warningLabel("     Welcome back applicant \n");
    print.warningLabel("≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠\n");
    print.inputLabel("Enter username : ");
    String username = console.nextLine();
    print.inputLabel("Enter password : ");
    String password = console.nextLine();
    Boolean verified = false;
    verified = parserCsv("applicant.csv", username, password);
    setUser(this.user_type, username);
    return verified;
  }

  public Boolean verifyAdmin() {
    Color print = new Color();
    print.inputLabel("Enter username : ");
    String username = console.nextLine();
    print.inputLabel("Enter password : ");
    String password = console.nextLine();
    Boolean verified = false;
    verified = parserCsv("admin.csv", username, password);
    if (verified) {
      this.user_type = "admin";
      setUser(this.user_type, username);
    }
    return verified;
  }

  public void setUser(String user_type, String username) {
    // set user in text file to able to move data freely
    PrintWriter writer;
    try {
      writer = new PrintWriter(System.getProperty("user.dir") + "/data/currentUser.txt");
      writer.print(username);
      writer.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  public Boolean parserCsv(String path,String username,String password){
    Boolean verifire = false;
    String line = "";
    String splitBy = ",";
    try {
      BufferedReader applicant_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/" + path));
      while ((line = applicant_list.readLine()) != null) {
        String[] applicant = line.split(splitBy);
        if (applicant[0].equals(username) && applicant[1].equals(password)) {
          verifire = true;
        }
      }
      applicant_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return verifire;
  }

  public static String banner() throws Exception {
    String path = System.getProperty("user.dir") + "/banner.txt";
    return new String(Files.readAllBytes(Paths.get(path)));
  }

  public final static void clearConsole(){
    System.out.print("\033[H\033[2J");  
    System.out.flush();
  }
}