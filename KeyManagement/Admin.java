
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.plaf.OptionPaneUI;

public class Admin {
  Scanner console = new Scanner(System.in);
  String username, password, name;

  public void adminMain() {
    Color print = new Color();
    getCurrentUser();
    welcomeHome();
    menu();

  }

  // menu
  public void menu() {
    getCurrentUser();
    Color print = new Color();
    print.warningLabel("What you want to do today?\n");
    print.successLabel("1. Pending Booking \t 2. Application Info \t 3. Generate Booking Report\t 4. Logout\n");
    print.inputLabel("Enter you option : ");
    String option = console.nextLine();
    Booking temp_booking = new Booking();
    if (option.equals("1")) {
      try {
        temp_booking.approveBooking();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (option.equals("2")) {
      Applicant temp_applicant = new Applicant();
      temp_applicant.applicantList();
    } else if (option.equals("3")) {
      try {
        temp_booking.generateReport();
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }
    }else if(option.equals("4")) {
      print.successLabel("Good Bye !!\n");
    }else {
      print.errorLabel("Wrong Option !!\n");
      print.errorLabel("Good Bye !!\n");
    }
  }
  
  // recheck current user
  public void getCurrentUser(){
    String line = "";
    try {
      BufferedReader applicant_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/currentUser.txt"));
      while ((line = applicant_list.readLine()) != null) {
        setUser(line); 
      }
      applicant_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }    
  }

  public void setUser(String username){
    parserCsv(username);
  }

  public void parserCsv(String username){
    String line = "";
    String splitBy = ",";
    try {
      BufferedReader admin_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/admin.csv"));
      while ((line = admin_list.readLine()) != null) {
        String[] admin = line.split(splitBy);
        if (admin[0].equals(username)) {
          this.username = admin[0];
          this.password = admin[1];
          this.name = admin[2];
        }
      }
      admin_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void welcomeHome(){
    Color print = new Color();
    print.inputLabel("Welcome back " + this.name + "\n");
  }
}