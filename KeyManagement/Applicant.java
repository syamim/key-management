
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Applicant {
  Scanner console = new Scanner(System.in);
  String username, password, name;
  
  public void ApplicantMain() {
    Color print = new Color();
    getCurrentUser();
    welcomeHome();
    menu();
  }

  public void welcomeHome(){
    Color print = new Color();
    print.inputLabel("Welcome back " + this.name + "\n");
  }

  public void menu() {
    getCurrentUser();
    Color print = new Color();
    print.warningLabel("What you want to do today?\n");
    print.successLabel("1. View Booked Date \t 2. Book Gym \t 3. Print My Bookin Slip\t 4. Exit\n ");
    print.inputLabel("Enter you option : ");
    String option = console.nextLine();
    Booking temp_booking = new Booking();
    if (option.equals("1")) {
      viewBooking();
    } else if (option.equals("2")) {
      temp_booking.addBooking(this.username, this.name);
    } else if (option.equals("3")) {
      temp_booking.printBookingSlip(this.username, this.name);
    }else if(option.equals("4")){
      print.successLabel("Good Bye !!\n");
    } else {
      print.errorLabel("Wrong Option !!\n");
      print.errorLabel("Good Bye !!\n");
    }
  }
  
  //********** Admin Module **********//
  public void applicantList(){
    Table table = new Table();
    table.printTable("applicant.csv");
    Admin admin = new Admin();
    admin.menu();
  }
  
  //************* Applicant module **********//
  // View Booking Module //
  public void viewBooking() {
    Table table = new Table();
    table.printTable("booking.csv");
    menu();
  }

  // set user data
  public void getCurrentUser(){
    String line = "";
    try {
      BufferedReader applicant_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/currentUser.txt"));
      while ((line = applicant_list.readLine()) != null) {
        setUser(line);
        
      }
      applicant_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }    
  }

  public void setUser(String username){
    parserCsv(username);
  }
  
  public void parserCsv(String username){
    String line = "";
    String splitBy = ",";
    try {
      BufferedReader applicant_list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/applicant.csv"));
      while ((line = applicant_list.readLine()) != null) {
        String[] applicant = line.split(splitBy);
        if (applicant[0].equals(username)) {
          this.username = applicant[0];
          this.password = applicant[1];
          this.name = applicant[2];
        }
      }
      applicant_list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}