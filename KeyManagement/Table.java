
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;



public class Table {
    private static final String HORIZONTAL_SEP = "-";
    private String verticalSep;
    private String joinSep;
    private String[] headers;
    private List<String[]> rows = new ArrayList<>();
    private boolean rightAlign;

    public Table() {
        setShowVerticalLines(false);
    }

    public void setRightAlign(boolean rightAlign) {
        this.rightAlign = rightAlign;
    }

    public void setShowVerticalLines(boolean showVerticalLines) {
        verticalSep = showVerticalLines ? "|" : "";
        joinSep = showVerticalLines ? "+" : " ";
    }

    public void setHeaders(String... headers) {
        this.headers = headers;
    }

    public void addRow(String... cells) {
        rows.add(cells);
    }

    public void print() {
        int[] maxWidths = headers != null ?
                Arrays.stream(headers).mapToInt(String::length).toArray() : null;

        for (String[] cells : rows) {
            if (maxWidths == null) {
                maxWidths = new int[cells.length];
            }
            if (cells.length != maxWidths.length) {
                throw new IllegalArgumentException("Number of row-cells and headers should be consistent");
            }
            for (int i = 0; i < cells.length; i++) {
                maxWidths[i] = Math.max(maxWidths[i], cells[i].length());
            }
        }

        if (headers != null) {
            printLine(maxWidths);
            printRow(headers, maxWidths);
            printLine(maxWidths);
        }
        for (String[] cells : rows) {
            printRow(cells, maxWidths);
        }
        if (headers != null) {
            printLine(maxWidths);
        }
    }

    private void printLine(int[] columnWidths) {
        for (int i = 0; i < columnWidths.length; i++) {
            String line = String.join("", Collections.nCopies(columnWidths[i] +
                    verticalSep.length() + 1, HORIZONTAL_SEP));
            System.out.print(joinSep + line + (i == columnWidths.length - 1 ? joinSep : ""));
        }
        System.out.println();
    }

    private void printRow(String[] cells, int[] maxWidths) {
        for (int i = 0; i < cells.length; i++) {
            String s = cells[i];
            String verStrTemp = i == cells.length - 1 ? verticalSep : "";
            if (rightAlign) {
                System.out.printf("%s %" + maxWidths[i] + "s %s", verticalSep, s, verStrTemp);
            } else {
                System.out.printf("%s %-" + maxWidths[i] + "s %s", verticalSep, s, verStrTemp);
            }
        }
        System.out.println();
    }
  
    //   Applicant Module
    public void printTable(String path){
    Table st = new Table();
    st.setShowVerticalLines(true);

    String line = "";
    String splitBy = ",";
    try {
      BufferedReader list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/" + path));
      String tableHeader = list.readLine();
      String[] headerName = tableHeader.split(splitBy);
      st.setHeaders("#",headerName[0],headerName[1], headerName[2]);
      Integer counter = 0;
      while ((line = list.readLine()) != null) {
        String[] item = line.split(splitBy);
        counter++;
        st.addRow(""+counter,item[0],item[1],item[2]);
      }
      list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    st.print();
  }
 
  
  public void printTableUser(String path, String name,Integer selected){
    Table st = new Table();
    st.setShowVerticalLines(true);
    String line = "";
    String splitBy = ",";
    try {
      BufferedReader list = new BufferedReader(
      new FileReader(System.getProperty("user.dir") + "/data/" + path));
      String tableHeader = list.readLine();
      String[] headerName = tableHeader.split(splitBy);
      st.setHeaders("#",headerName[0],headerName[1], headerName[2]);
      int counter = 0;
      while ((line = list.readLine()) != null) {
        String[] item = line.split(splitBy);
        if (item[0].equals(name)) {
            if (selected == 0){
                counter++;
                st.addRow(""+counter,item[0],item[1],item[2]);
            }else if (selected > 0){
                counter++;
                if (counter == selected){
                    PrintWriter report = new PrintWriter(System.getProperty("user.dir") + "/data/report/booking_slip.txt");
                    report.println("Booking Slip Report");
                    report.println("");
                    report.println("Applicant Detail");
                    report.println("Name : " + item[0]);
                    report.println("Booking From : " + item[1] + "until" + item[2]);
                    if (item[3].equals("false")){
                        report.println("Approve : Not Approve By Lecturer Yet.");
                    }else{
                        report.println("Approve : Approved.");
                    }
                    report.close();
                    break;
                }
            }
        }
      }
      list.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    if (selected == 0){
        st.print();
    }
  }

    //   Admin Module
    // print unapproved table
    public void adminPrintTable(String path,Integer selected){
        Table st = new Table();
        st.setShowVerticalLines(true);

        String line = "";
        String splitBy = ",";
        try {
            BufferedReader list = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/data/" + path));
            String tableHeader = list.readLine();
            String[] headerName = tableHeader.split(splitBy);
            st.setHeaders("#",headerName[0],headerName[1], headerName[2], headerName[3]);
            Integer counter = 0;
            while ((line = list.readLine()) != null) {
                String[] item = line.split(splitBy);
                if (selected == 0){
                    if(item[3].equals("false")){
                        st.addRow(""+counter,item[0],item[1],item[2],"Not Approved Yet");
                    }else{
                        st.addRow(""+counter,item[0],item[1],item[2],"Approved");
                    }
                }else if(selected != 0){
                    counter++;
                    if(item[3].equals("false")){
                        st.addRow(""+counter,item[0],item[1],item[2],"Not Approved Yet");
                    }else{
                        st.addRow(""+counter,item[0],item[1],item[2],"Approved");
                    }
                }
            }
            list.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        st.print();
    }

}